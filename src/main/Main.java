package main;

import exercicios.Exercicio1;
import exercicios.Exercicio2;
import exercicios.Exercicio3;
import exercicios.Exercicio4;

public class Main {

	public static void main(String[] args) {

		Exercicio1 resultado1 = new Exercicio1();
		Exercicio2 resultado2 = new Exercicio2();
		Exercicio3 resultado3 = new Exercicio3();
		Exercicio4 resultado4 = new Exercicio4();

		resultado1.soma();
		resultado2.opera��es();
		resultado3.consumo();
		resultado4.vendas();

	}

}
